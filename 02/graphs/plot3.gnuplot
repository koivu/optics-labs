set terminal epslatex
set output "./plot3.tex"

set xlabel font "Libertine, 16"
set ylabel font "Libertine, 16"
set xlabel "\\Large $\\sin \\varphi$"
set xlabel offset 0,-1
set ylabel "\\Large $I_\\varphi$"
#set key box
set key off
set yzeroaxis
set ytics nomirror out
set xtics nomirror out
set xtics 1
set xtics add ("$-\\cfrac{6\\lambda}{d}$" -6, "$-\\cfrac{5\\lambda}{d}$" -5, "$-\\cfrac{4\\lambda}{d}$" -4, "$-\\cfrac{3\\lambda}{d}$" -3, "$-\\cfrac{2\\lambda}{d}$" -2, "$-\\cfrac{\\lambda}{d}$" -1, "$0$" 0, "$\\cfrac{\\lambda}{d}$" 1, "$\\cfrac{2\\lambda}{d}$" 2, "$\\cfrac{3\\lambda}{d}$" 3, "$\\cfrac{4\\lambda}{d}$" 4, "$\\cfrac{5\\lambda}{d}$" 5, "$\\cfrac{6\\lambda}{d}$" 6)
set xtics offset 0, -0.3
set border 3
set format x ""
set format y ""
#set grid x y
set xrange [-6:6]
#set yrange [-0.01:11]
set samples 3000
#Parameters
d = 2.5 * 10E-6
b = d/3
lambda = 640*10E-9
N = 4
u = 3.14 * b / lambda
del = 3.14 * d / lambda
t = lambda / d
#
f(x) = (sin(u*x)*sin(N*del*x)/(u*x*sin(del*x)))**2
g(x) = N*N*(sin(u*x)/(u*x))**2
#
plot f(x*t) ls 1 lw 2, g(x*t) lw 2
