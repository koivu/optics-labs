set terminal epslatex
set output "./plot1.tex"

set xlabel font "Libertine, 16"
set ylabel font "Libertine, 16"
set xlabel "\\Large $\\delta$"
set xlabel offset 0,-1
set ylabel "\\Large $I_\\varphi$"
set key off
set yzeroaxis
set ytics nomirror out
set xtics nomirror out
set border 3
set format x ""
set format y ""
set xrange [-pi/3:7*pi/3]
set xtics ("$-\\cfrac{\\pi}{3}$" -pi/3, "0" 0, "$\\cfrac{\\pi}{3}$" pi/3, "$\\cfrac{\\pi}{2}$" pi/2, "$\\cfrac{2\\pi}{3}$" 2*pi/3, "$\\cfrac{5\\pi}{6}$" 5*pi/6, "$\\pi$" pi, "$\\cfrac{7\\pi}{6}$" 7*pi/6, "$\\cfrac{4\\pi}{3}$" 4*pi/3, "$\\cfrac{9\\pi}{6}$" 9*pi/6, "$\\cfrac{5\\pi}{3}$" 5*pi/3, "$2\\pi$" 2*pi, "$\\cfrac{7\\pi}{3}$" 7*pi/3)
set xtics offset 0, -0.3
set samples 3000
#Parameters
d = 2.5 * 10E-6
b = d/3
lambda = 640*10E-9
N1 = 6
N2 = 2
u = pi * b / lambda
del = pi * d / lambda
#t = lambda / b
t = pi / 3
#
g(x) = (sin(N1*x*3/(2*pi))/sin(x*3/(2*pi)))**2
f(x) = 9*(sin(N2*x*3/(2*pi))/sin(x*3/(2*pi)))**2
#
set label "N=2" at pi/5, 34
set label "N=6" at pi/6, 20
plot g(x*t) ls 1 lw 2, f(x*t) ls 1 lw 2
