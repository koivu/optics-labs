set terminal epslatex
set output "./plot2.tex"

set xlabel font "Libertine, 16"
set ylabel font "Libertine, 16"
set xlabel "\\Large $\\sin \\varphi$"
set xlabel offset 0,-1
set ylabel "\\Large $I_\\varphi$"
#set key box
set key off
#set yzeroaxis
set ytics nomirror out
set xtics nomirror out
set border 3
set xtics 1

set xtics add ("$-3\\cfrac{\\lambda}{b}$" -3, "$-2\\cfrac{\\lambda}{b}$" -2, "$-\\cfrac{\\lambda}{b}$" -1)
set xtics offset graph -1, -0.3

set xtics add ("$0$" 0, "$\\cfrac{\\lambda}{b}$" 1, "$2\\cfrac{\\lambda}{b}$" 2, "$3\\cfrac{\\lambda}{b}$" 3)
set xtics offset 0, -0.3

set format x ""
set format y ""
set xrange [-3:3]
#set yrange [-0.01:11]
set samples 3000
#Parameters
d = 2.5 * 10E-6
b = d/3
lambda = 640*10E-9
N = 4
u = 3.14 * b / lambda
del = 3.14 * d / lambda
t = lambda / b
#
g(x) = (sin(u*x)/(u*x))**2
plot g(x*t) ls 1 lw 2
