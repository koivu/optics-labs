\textit{Целью работы} является изучение интерференции света, определение радиуса кривизны сферической поверхности.

\textit{Обеспечивающие средства:} ртутная лампа, микроскоп, линза.

\section{ТЕОРЕТИЧЕСКОЕ ВВЕДЕНИЕ}
\subsection{Интерференция света. Кольца Ньютона.}
Волновые свойства света наиболее отчётливо проявляют себя в явлениях интерференции и дифракции. Под интерференцией света обычно понимают широкий круг явлений, в которых при наложении световых волн результирующая интенсивность не равна сумме интенсивностей отдельных волн: в одних точках пространства она больше, в других~--- меньше, то есть возникают чередующиеся светлые и тёмные участки~--- интерференционные полосы. При интерференции происходит только перераспределение энергии световой волны в пространстве, в то время как полный поток энергии остается неизменным.

Необходимым и достаточным условием возникновения интерференции при сложении монохроматических волн является равенство их периодов, а значит, и длин волн, либо частот. Реальные световые волны не являются строго монохроматичными, поэтому условия равенства их частот уже недостаточно для наблюдения интерференции. В~этом случае следует потребовать постоянства во времени разности фаз складывающихся колебаний~---~когерентности. Кроме этого, следует отметить, что при соблюдении данных условий интерференция не будет иметь места, если световые волны ортогонально поляризованы.

Свет от обычных (нелазерных) источников излучения никогда не бывает монохроматическим. Это связано с механизмом испускания света атомами источника излучения. Атом может находиться в возбуждённом состоянии около $\tau = 10^{-8}$~с, и примерно столько же длится процесс испускания света. Поэтому волна, излучаемая отдельным атомом, может быть в первом приближении представлена в виде волнового цуга~--- обрывка синусоиды. Длина цуга $l \approx c \cdot \tau = 3\cdot 10^8 \times 10^{-8} = 3$~м (здесь $c = 3\cdot 10^8$~м/с~--- скорость света в вакууме). Так как свет испускается одновременно огромным количеством атомов и излучают они независимо друг от друга, реальная световая волна представляет собой хаотическую последовательность отдельных цугов синусоидальных волн с беспорядочно меняющейся фазой. Поэтому при наложении пучков света от разных источников фазовые соотношения между световыми колебаниями в любой точке за время наблюдения успевают многократно измениться случайным образом.

В результате сложения большого числа колебаний со случайными фазами энергия результирующего колебания в любой точке пространства будет равна сумме энергий отдельных колебаний, т.\,е. характерного для интерференции перераспределения энергии в пространстве не происходит. Отсюда ясно, что для наблюдения интерференции света необходимы специальные условия: свет от одного и того же источника нужно разделить на два пучка и затем наложить их друг на друга подходящим способом. Если разность хода этих пучков от источника излучения до точки наблюдения не превышает длины отдельного цуга~$l$, то случайные изменения амплитуды и фазы световых колебаний в этих двух пучках происходят согласованно, т.\,е. эти изменения коррелируют между собой. О~таких пучках говорят, что они полностью или частично когерентны, в зависимости от того, будет ли эта корреляция полной или частичной.

Экспериментально когерентные пучки можно получить из одного светового пучка делением волнового фронта или делением амплитуды. В~методе деления волнового фронта пучок пропускается либо через два близко расположенных отверстия в непрозрачном экране, либо отражаясь от зеркальных
поверхностей. Такой метод пригоден лишь при достаточно малых размерах источника излучения. В~методе деления амплитуды пучок делится на одной или нескольких частично отражающих, частично пропускающих поверхностях. Метод деления амплитуды может применяться и при протяжённых источниках. Он обеспечивает большую интенсивность и лежит в основе действия разнообразных интерферометров.

Рассмотрим более подробно метод деления амплитуды для получения когерентных волн. Пусть световая волна от источника падает на тонкую прозрачную пластинку. В этом случае происходит отражение волны от передней и задней поверхностей пластинки, в результате чего возникают две световые волны, способные при наложении интерферировать. При освещении пластинки постоянной толщины рассеянным светом, в котором содержатся лучи различных направлений, наблюдаются локализованные в бесконечности интерференционные полосы равного наклона. При освещении пластинки переменной толщины параллельными лучами света, наблюдаются интерференционные полосы равной толщины. Локализованы полосы равной толщины вблизи поверхности пластинки. Их можно наблюдать невооруженным глазом или с помощью сфокусированного на поверхность пластинки микроскопа.

%\begin{figure}[ht]
\begin{wrapfigure}[11]{l}{0.4\linewidth}
\includegraphics[width=1\linewidth]{04/pic1.pdf}
\caption{Геометрия задачи.}
\label{im:1_04}
\end{wrapfigure}

Классическим примером полос равной толщины являются кольца Ньютона. Они наблюдаются при отражении света от соприкасающихся друг с другом поверхностей плоского стекла и плоско-выпуклой линзы с большим радиусом кривизны~(рис.~\ref{im:1_04}). Роль тонкой плёнки, от которой отражаются когерентные волны, играет воздушный зазор между поверхностью стекла и линзой. При нормальном падении света (вертикально сверху) полосы равной толщины имеют вид концентрических окружностей, при наклонном падении света~--- эллипсов.

%\begin{figure}[ht]
\begin{wrapfigure}[14]{l}{0.4\linewidth}
\includegraphics[width=1\linewidth]{04/pic2.pdf}
\caption{Наложение световых лучей.}
\label{im:2_04}
\end{wrapfigure}

Найдем радиусы колец Ньютона, получающихся при падении монохроматического света по нормали к стеклянной пластинке. Разность хода $\Delta$ между лучами света~1~и~2~(рис.~\ref{im:2_04}) в силу большого радиуса кривизны $R$ можно считать равной:
\begin{equation}
	\label{eq:1_04}
	\Delta = 2\delta + \frac{\lambda}{2},
\end{equation}
где $\delta$~---~толщина воздушного зазора, увеличивающаяся по мере удаления от центра линзы; $\lambda$~---~длина волны падающего монохроматического света; ${\lambda}/{2}$~--- дополнительная разность хода, возникающая при отражении луча~$1$ от поверхности стеклянной пластины, показатель преломления которой больше, чем показатель преломления воздуха.

Из~рис.~\ref{im:1_04} видно, что толщина воздушного зазора $\delta$ связана с радиусами наблюдаемых колец $r_k$ и радиусом кривизны линзы $R$ следующим образом:
\begin{align}
	\label{eq:2_04}
	R^2 = r_k^2 + (R-\delta)^2 =& \, r_k^2 + R^2\left(1 - \frac{\delta}{R} \right)^2 = \\ = & \, r_k^2 + R^2\left( 1 - 2 \frac{\delta}{R} + \left(\frac{\delta}{R}\right)^2 \right).
\end{align}

Учитывая, что $\left(\cfrac{\delta}{R}\right)^2\! \ll 1$, и пренебрегая этим слагаемым в формуле~(\ref{eq:2_04}), получим:
\begin{equation}
	\label{eq:3_04}
	r_k^2 = 2\delta R, \qquad 2\delta = \frac{r_k^2}{R}.
\end{equation}

С учётом (\ref{eq:3_04}) получим из (\ref{eq:1_04}):
\begin{equation}
	\label{eq:4_04}
	\Delta = 2\delta + \frac{\lambda}{2} = \frac{r_k^2}{R} + \frac{\lambda}{2}.
\end{equation}

Интерференционные максимумы (светлые кольца) получаются в случае, когда на разности хода укладывается чётное число полуволн:
\begin{equation}
	\label{eq:5_04}
	\Delta = k\cdot\lambda = 2k\cdot \frac{\lambda}{2},
\end{equation}
где $k = 0,1,2,\ldots$~---~порядок интерференции, или номер соответствующего кольца.

Интерференционные минимумы (тёмные кольца) получаются, если на разности хода укладывается нечётное число полуволн:
\begin{equation}
	\label{eq:6_04}
	\Delta = (2k+1)\cdot\frac{\lambda}{2}.
\end{equation}

Из формул~(\ref{eq:5_04})~и~(\ref{eq:6_04}) видно, что в центре интерференционной картины при $k = 0$ должно наблюдаться тёмное пятно, так как в этом случае толщина воздушного зазора $\delta \to 0$, и происходит лишь потеря полуволны от плоской стеклянной пластинки.

Принимая во внимание условие интерференционных минимумов~(\ref{eq:6_04}) и формулу~(\ref{eq:4_04}), получаем:
\begin{equation}
	\label{eq:7_04}
	\frac{r_k^2}{R} + \frac{\lambda}{2} = (2k+1)\cdot\frac{\lambda}{2} = k\cdot\lambda + \frac{\lambda}{2}.
\end{equation}

Следовательно, выражение для радиусов тёмных колец имеет вид:
\begin{equation}
	\label{eq:8_04}
	r_k^2 = k\cdot\lambda \cdot R.
\end{equation}

Аналогично для светлых колец получаем:
\begin{equation}
	\label{eq:9_04}
	r_k^2 = (2k-1)R\cdot\frac{\lambda}{2}.
\end{equation}

Если падающий свет является немонохроматическим (белым), то кольца окажутся спектрально окрашенными: внутренняя часть кольца окажется сине-фиолетовой, наружная~---~красной.

\section{ПРАКТИЧЕСКАЯ ЧАСТЬ}

\subsection{Определение радиуса кривизны линзы}

%\begin{figure}[ht]
\begin{wrapfigure}[15]{l}{0.45\linewidth}
\includegraphics[width=1\linewidth]{04/pic3.pdf}
\caption{Схема лабораторной установки.}
\label{im:3_04}
\end{wrapfigure}

Из формулы~(\ref{eq:8_04}) видно, что по известным значениям длины волны падающего света $\lambda$ и радиусов темных колец Ньютона $r_k$, можно определить радиус кривизны линзы $R$.

Радиусы тёмных колец измеряются при помощи микроскопа~(рис.~\ref{im:3_04}). На~предметный столик микроскопа устанавливаются линза и затемнённая стеклянная пластинка, помещённые в специальную оправу. В~качестве источника света~$S$ используется ртутная лампа. Свет от ртутной лампы проходит через зелёный светофильтр, пропускающий излучение с длиной волны $\lambda = 5.46 \cdot 10^{-5}$~см, и падает на поворотную прозрачную пластинку, укреплённую на оправе линзы. Поворотная пластинка установлена под углом $45^\circ$ к плоской поверхности линзы, тем самым обеспечивая нормальное падение света (вертикально сверху) на ее поверхность. Кольца Ньютона наблюдаются через микроскоп, сфокусированный на выпуклую поверхность линзы. В~центре интерференционной картины находится тёмное пятно.

Диаметры тёмных колец Ньютона измеряются с помощью микрометрического винта, расположенного с правой стороны микроскопа. Для этого необходимо установить перекрестие окуляра микроскопа по центру темного пятна и, поворачивая микрометрический винт, переместить перекрестие окуляра в левую часть интерференционной картины таким образом, чтобы центр перекрестия совпадал не менее, чем (приблизительно) с пятнадцатым темным кольцом Ньютона. Записать показания деления шкалы микроскопа, которая находится за его тубусом (цена деления 1 мм), и показания микрометрического винта, расположенного справа от микроскопа (100 делений на его шкале соответствуют 1 мм).

Затем следует повернуть микрометрический винт так, чтобы совместить центр перекрестия с четырнадцатым тёмным кольцом. Снова записать показания шкалы микроскопа и деления микрометрического винта, перейти к тринадцатому кольцу и т.\,д. Микрометрический винт необходимо поворачивать только в одну сторону.

Положения тёмных колец Ньютона записываются сначала в порядке убывания их номера, а после прохождения центрального тёмного кольца, которое необходимо дополнительно отметить, в порядке возрастания (также примерно до пятнадцатого кольца).

Далее следует приступить к обработке измерений. Найти по разности показаний диаметр отмеченного центрального тёмного кольца.

Разность следующих показаний, ближайших к центральному пятну справа и слева, равна диаметру первого темного кольца Ньютона. Следующая разность показаний равна диаметру второго кольца и т.\,д.
Такой метод измерений позволяет избежать ошибки в определении диаметра кольца определённого номера, так как проведении измерений нет необходимости нумеровать кольца (нумерация проводится при обработке экспериментальных данных).

По известным диаметрам найти радиусы тёмных колец Ньютона (в~сантиметрах):
\begin{equation}
	\label{eq:10_04}
	r_k = \frac{d_k}{2}.
\end{equation}

Построить график зависимости $r_k^2$ от номера тёмного кольца $k$ (центральное кольцо при построении графика не учитывать). Эта зависимость должна иметь линейный характер~(рис.~\ref{im:4_04}).

\begin{figure}[ht]
\includegraphics[width=0.65\linewidth]{04/pic4.pdf}
\caption{График зависимости $r_k^2$ от номера тёмного кольца $k$.}
\label{im:4_04}
\end{figure}

Тангенс угла наклона $\tg \alpha$ полученной прямой находится из выражения:
\begin{equation}
	\label{eq:11_04}
	\tg \alpha = \frac{r_m^2 - r_n^2}{k_m - k_n}.
\end{equation}

С другой стороны, согласно формуле~(\ref{eq:8_04}), коэффициент пропорциональности между $r_k^2$ и номером тёмного кольца $k$ равен:
\begin{equation}
	\label{eq:12_04}
	\tg \alpha = \lambda \cdot R,
\end{equation}
откуда следует, что:
\begin{equation}
	\label{eq:13_04}
	R = \frac{\tg \alpha}{\lambda} = \frac{r_m^2 - r_n^2}{(k_m - k_n)\lambda}.
\end{equation}

В настоящей работе значение радиуса кривизны линзы R определяется в сантиметрах.

\section{ПОРЯДОК ВЫПОЛНЕНИЯ РАБОТЫ}
\textbf{ВНИМАНИЕ!} В работе применяется ртутная лампа, являющаяся мощным источником ультрафиолетового излучения. \textbf{Запрещается смотреть непосредственно на выходное окно ртутной лампы}, т.\,к. при прямом попадании света в глаза возможен ожог сетчатки глаза.

\begin{enumerate}
\item Включить ртутную лампу, в течение 5 минут прогреть её при значении силы
тока $1.8$~А.
\item Установить значение силы тока ртутной лампы $1.2$~А. Перед выходным окном лампы должен стоять зеленый светофильтр.
\item  Если при наблюдении в микроскоп чётко видны кольца Ньютона, сразу приступить к выполнению пункта 11. Если кольца Ньютона не наблюдаются, выполнить пункты 4-10.
\item Поместить специальную оправу с затемнённой пластинкой и линзой на предметный столик микроскопа.
\item Поворотную пластинку, укреплённую на этой оправе, установить со стороны ртутной лампы под углом $45^\circ$ к плоской поверхности линзы, чтобы обеспечить нормальное падение света (вертикально сверху) на линзу.
\item При необходимости изменить высоту ртутной лампы так, чтобы свет от неё равномерно освещал поворотную пластинку.
\item Вращая микрометрический винт микроскопа, установить окуляр микроскопа над поворотной пластинкой.
\item Между линзой и затемнённой пластинкой положить листок бумаги и сфокусировать на него микроскоп. Изображение листка бумаги должно быть видно в окуляр.
\item Убрать листок бумаги. В точке соприкосновения линзы с затемнённой пластинкой должно наблюдаться тёмное пятно. Сфокусировать микроскоп на тёмное пятно и рассмотреть систему колец Ньютона.
\item При необходимости, смещая поворотную пластинку, добиться чёткой интерференционной картины.
\item Установить перекрестие окуляра микроскопа по центру тёмного пятна и, поворачивая микрометрический винт, переместить перекрестие окуляра в левую часть таким образом, чтобы центр перекрестия совпадал не менее, чем (приблизительно) с пятнадцатым тёмным кольцом Ньютона.
\item Записать показания деления шкалы микроскопа, которая находится за его тубусом и выражена в миллиметрах, и показания микрометрического винта, расположенного справа от микроскопа (100 делений на шкале микрометрического винта микроскопа соответствуют 1 мм).
\item Повернуть микрометрический винт так, чтобы совместить центр перекрестия с четырнадцатым тёмным кольцом. Снова записать показания шкалы микроскопа и деления микрометрического винта, перейти к тринадцатому кольцу и т. д. Микрометрический винт необходимо поворачивать только в одну сторону.
\item Положения тёмных колец Ньютона записываются сначала в порядке убывания их номера, а после прохождения центрального темного пятна, которое необходимо дополнительно отметить, в порядке возрастания (также примерно до пятнадцатого кольца).
\item Найти по разности полученных показаний диаметр отмеченного центрального тёмного кольца. Разность следующих показаний, ближайших к центральному пятну справа и слева, равна диаметру первого тёмного кольца Ньютона. Следующая разность показаний равна диаметру второго кольца и т.\,д. Таким образом найти диаметр других (приблизительно до пятнадцатого) колец Ньютона.
\item По известным диаметрам найти радиусы тёмных колец Ньютона по формуле~\eqref{eq:10_04}. Перевести эти значения из миллиметров в сантиметры.
\item Рассчитать квадраты радиусов темных колец Ньютона $r_k^2$.
\item 	Построить график зависимости $r_k$ от номера тёмного кольца $k$ (центральное пятно при построении графика не учитывать).
\item По формуле~(\ref{eq:11_04}) найти тангенс угла наклона $\tg \alpha$ полученной прямой.
\item Рассчитать радиус кривизны линзы $R$ по формуле~(\ref{eq:13_04}). Длина волны падающего света $\lambda = 5.46 \cdot 10^{-5}$~см.
\end{enumerate}

\section{КОНТРОЛЬНЫЕ ВОПРОСЫ}
\begin{enumerate}
\item В чем заключается явление интерференции света?
\item Какие волны способны интерферировать?
\item Почему нельзя получить интерференционную картину при наложении световых волн от разных источников излучения?
\item Какие методы используются в оптике для получения когерентных волн?
\item Нарисуйте оптическую схему установки для наблюдения колец Ньютона и укажите ход интерферирующих лучей.
\item Где локализована интерференционная картина?
\item Напишите формулу для нахождения разности хода между лучами и поясните её.
\item Напишите условия наблюдения интерференционных максимумов и минимумов.
\item Почему интерференционная картина имеет вид окружностей?
\item Выведите формулу для радиусов тёмных колец Ньютона.
\item Какое пятно наблюдается в центре интерференционной картины? Почему?
\item Каким образом определяется радиус кривизны линзы?
\end{enumerate}

\clearpage