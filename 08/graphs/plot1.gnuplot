#! /usr/bin/gnuplot -persist
set terminal epslatex
set output "plot1.tex"
set encoding utf8
set xlabel font "Libertine, 16"
set ylabel font "Libertine, 16"
set xlabel "\\Large $\\lambda$, нм"
set ylabel "\\Large f($\\lambda$, T), $10^{13}$ Вт/м$^3$"
set key off
set format y "%2.1e"
set grid x y
set xrange [100E-9:1600E-9]
set yrange [0:1.2E14]
set xtics ("100" 100E-9, "400" 400E-9, "700" 700E-9, "1000" 1000E-9, "1300" 1300E-9, "1600" 1600E-9)
set ytics ("0" 0, "2" 2E+13, "4" 4E+13, "6" 6E+13, "8" 8E+13, "10" 10E+13)
set samples 3000
#dashed area
#set object 1 rectangle fc lt -1 fs transparent pattern 4 bo noborder behind
#set object 1 rectangle from 400E-9, 3.5E5 to 700E-9, 0
#Planks formulas
A=2*pi*9*(10**16)*6.62*(10**(-34))
B=6.62*(10**(-34))*3*(10**8)/(1.38*10**(-23))
#
#temp(x) = (x<700E-9)?0:10E20
f(x) = A/((x**5)*(exp(B/(x*6000))-1))
#g(x) = (x<400E-9)?10E20:temp(x)
#
#set style fill pattern 2
#
#set style line 1 lt 1 lw 3 pt 3 linecolor rgb "black"
#
set label "6000K" at 500E-9, 1.02E14 front
set label "5500K" at 500E-9, 6.9E13 front
set label "5000K" at 500E-9, 4.4E13 front
set label "4500K" at 500E-9, 2.9E13 front
#
#plot f(x) title "6000K",\
#'+' using 1:(f($1)):(g($1)) notitle with filledcurves above lt rgb "gray",\
#f(x) notitle ls 1,\
#A/((x**5)*(exp(B/(x*5500))-1)) title "5500K" lt 1 lw 3,\
#A/((x**5)*(exp(B/(x*5000))-1)) title "5000K" lt 1 lw 3,\
#A/((x**5)*(exp(B/(x*4500))-1)) title "4500K" lt 1 lw 3

plot f(x) lt 1 lw 3, A/((x**5)*(exp(B/(x*5500))-1)) lt 1 lw 3, A/((x**5)*(exp(B/(x*5000))-1)) lt 1 lw 3, A/((x**5)*(exp(B/(x*4500))-1)) lt 1 lw 3
