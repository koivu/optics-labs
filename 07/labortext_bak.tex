\textit{Целью работы} является изучение законов фотоэффекта, освоение методики градуировки монохроматора, определение относительной чувствительности фотоэлемента.

\textit{Обеспечивающие средства:} универсальный монохроматор УМ-2 с оптическим окуляром и съемной выходной щелью, микровольтметр В2-11, ртутная лампа, осветитель, конденсорная линза.

\section{ТЕОРЕТИЧЕСКОЕ ВВЕДЕНИЕ}
\subsection{Фотоэффект}
Воздействие света на вещество сводится к передаче этому веществу энергии, переносимой световой волной, в результате чего могут возникнуть различные эффекты. Одним из них является фотоэлектрический эффект (фотоэффект).

В настоящее время различают три вида фотоэлектрического эффекта: внешний, внутренний и фотогальванический (фотоэффект в запирающем слое, или вентильный фотоэффект).

Внешний фотоэффект заключается в испускании поверхностью металлов электронов во внешнее пространство (вакуум или газ) под действием падающего на эту поверхность потока световой энергии.
Опытным путем были установлены три закона внешнего фотоэффекта:
\begin{enumerate}
\item При фиксированной частоте излучения число электронов (фотоэлектронов),
вырываемых с поверхности металла за единицу времени, прямо пропорционально плотности светового потока.
\item Максимальная начальная кинетическая энергия фотоэлектронов определяется
частотой падающего света и не зависит от его интенсивности.
\item Для каждого металла существует красная граница фотоэффекта, т.\,е. максимальная длина волны~$\lambda_0$ (минимальная частота~$\nu_0$), при которой ещё возможен фотоэффект, независимо от плотности светового потока и продолжительности облучения.
\end{enumerate}

Для объяснения законов фотоэффекта Эйнштейн предположил, что поток энергии световой волны не является непрерывным, а представляет собой поток дискретных порций энергии, называемых квантами или фотонами. Энергия фотона, соответствующая свету с частотой $\nu$, равна:
\begin{equation}
	\label{eq:01}
	E = h\nu,
\end{equation}
где $h = 6.62\cdot10^{-34}$~Дж$\cdot$с~--- постоянная Планка.

Фотон, столкнувшись с электроном в металле, передает ему всю свою энергию. Если эта энергия достаточно велика, то электрон может преодолеть удерживающие его силы и выйти из металла. В этом процессе соблюдается закон сохранения энергии, который можно записать в виде:
\begin{equation}
	\label{eq:02}
	E = A_{\text{вых}} + \frac{mv^2}{2},
\end{equation}
где $A_{\text{вых}}$~--- работа выхода (работа, совершенная электроном для преодоления сил, удерживающих его в объеме металла), $\cfrac{mv^2}{2}$~--- максимальная кинетическая энергия вылетевшего электрона. Соотношение~(\ref{eq:02}) называется уравнением Эйнштейна для фотоэффекта.

Из формулы~(\ref{eq:02}) следует, что в случае, когда работа выхода $A_{\text{вых}}$ превышает энергию кванта $h\nu$, электрон не сможет выйти за пределы металла. Следовательно, для возникновения фотоэффекта необходимо выполнение условия: $h\nu > A_{\text{вых}}$. Этим объясняется наличие красной границы, т.\,е. максимальной длины волны $\lambda_0$ или минимальной частоты $$\nu_0 = \cfrac{A_{\text{вых}}}{h},$$ при которой ещё возможен фотоэффект. Так как $$\nu_0 = \cfrac{c}{\lambda_0},$$ то
\begin{equation}
	\label{eq:03}
	\lambda_0 = \frac{hc}{A_{\text{вых}}},
\end{equation}
где $c = 3\cdot 10^8$~м/c~--- скорость света в вакууме.

\begin{figure}[t]
%\begin{wrapfigure}[14]{l}{0.6\textwidth}
\includegraphics[width=0.5\textwidth]{pictures/pic1.pdf}
\caption{Схема включения вакуумного фотоэлемента}
\label{im:01}
\end{figure}

{\itshape Внешний фотоэффект} используется в вакуумных фотоэлементах. Внутренняя поверхность баллона покрыта тонким слоем металла. Этот слой занимает примерно 50\% всей внутренней поверхности баллона и является катодом (фотокатодом). Против него оставляют прозрачное окно обычно из кварцевого стекла, через которое на катод попадает свет. Анод имеет форму рамки и расположен так, чтобы не препятствовать попаданию света на катод. Схема включения фотоэлемента изображена на~рис.~\ref{im:01}.

\begin{figure}[h]
%\begin{wrapfigure}[14]{l}{0.45\textwidth}
\includegraphics[width=.45\textwidth]{graphs/texplot1.pdf}
\caption{Вольт-амперная характеристика вакуумного фотоэлемента}
\label{im:02}
\end{figure}

Между катодом $K$ и анодом $A$ создается регулируемая потенциометром $R$ разность потенциалов~--- напряжение $U$, измеряемое вольтметром $V$. Сила тока $I$, проходящего между анодом и катодом, определяется миллиамперметром $mA$. При освещении фотоэлемента начинается эмиссия электронов с катода и в цепи возникает ток, получивший название фототока. На~рис.~\ref{im:02} показана вольт-амперная характеристика вакуумного фотоэлемента.

Как видно из графика, сначала фототок линейно увеличивается при увеличении анодного напряжения, так как при этом все большее количество вылетевших с катода электронов достигает анода. При некотором напряжении на аноде все фотоэлектроны попадают на анод и при дальнейшем увеличении напряжения сила тока не изменяется. Этот ток называется током насыщения. Сила тока насыщения $I_s$ прямо пропорциональна падающему световому потоку $\Phi$:
\begin{equation}
	\label{eq:04}
	I \sim \Phi.
\end{equation}

{\itshape Внутренним фотоэффектом} называется изменение электрической проводимости некоторых кристаллических тел (полупроводников) вследствие появления под действием потока световой энергии внутри всех этих тел добавочных электронов проводимости.

Особый практический интерес представляет вентильный фотоэффект (фотогальванический эффект), возникающий при освещении контакта полупроводников с $p$--~и~$n$--~проводимостью. Сущность этого явления заключается в следующем: при контакте полупроводников $p$-~и~$n$-типа создаётся контактная разность потенциалов, которая препятствует дальнейшему переходу основных носителей через контакт: дырок~--- в $n$-область и электронов~--- в $p$-область. При освещении $p$-$n$--перехода и прилегающих к нему областей в полупроводниках наблюдается внутренний фотоэффект, т.\,е. образуются электронно-дырочные пары. Под действием электрического поля $p$-$n$-перехода образовавшиеся заряды разделяются: неосновные носители проникают через переход, а основные задерживаются в своей области, в результате чего накапливаются заряды и на $p$-$n$--переходе создаётся добавочная разность потенциалов (фотоэлектродвижущая сила).

Фотоэлектродвижущая сила, возникающая при освещении контакта монохроматическим потоком света, пропорциональна его интенсивности, так как она определяется числом образующихся электронно-дырочных пар, т.\,е. количеством фотонов.

Преимущество вентильных фотоэлементов заключается в том, что для их работы не требуется источник питания, так как в них самих под действием света генерируется электродвижущая сила. Если замкнуть цепь, содержащую фотоэлемент, то в ней возникнет ток.

Вентильные фотоэлементы изготавливают на основе селена, германия, кремния, сернистого серебра и др. Кремниевые и некоторые другие типы фотоэлементов используются для солнечных батарей, применяемых на космических кораблях для питания бортовой аппаратуры, а также в фотометрии для измерения светового потока и освещенности.

\subsection{Чувствительность фотоэлемента}

%\begin{figure}[h]
\begin{wrapfigure}[9]{l}{0.35\textwidth}
\includegraphics[width=1\textwidth]{pictures/pic2.pdf}
\caption{Устройство фотоэлемента}
\label{im:03}
\end{wrapfigure}

Рассмотрим устройство и принцип действия используемого в настоящей работе селенового фотоэлемента~(рис.~\ref{im:03}). Селеновый фотоэлемент представляет собой слой селена, нанесенный на полированную железную пластинку. При прогревании селен переводится в кристаллическую модификацию, обладающую дырочной проводимостью. Сверху напыляется тонкая плёнка серебра. В результате диффузии атомов серебра внутрь селена образуется слой селена с примесью, обладающей электронной проводимостью. Таким образом, создается контакт между чистым селеном и селеном с примесью, то есть возникает $p$-$n$--переход. При освещении фотоэлемента свет легко проходит через тонкую пленку серебра. Фотоны поглощаются электронами, и возникает электродвижущая сила. Если соединить проводником железную пластинку с плёнкой серебра, то гальванометр $\text{Г}$, включенный в цепь, покажет силу тока, протекающего между электродами.

Различают интегральную и спектральную чувствительности фотоэлемента. Интегральная чувствительность $\gamma$ характеризует способность фотоэлемента реагировать на воздействие светового потока сложного спектрального состава:
\begin{equation}
	\label{eq:05}
	\gamma = \frac{I}{\Phi},
\end{equation}
где $I$~--- величина фототока, $\Phi$~--- поток энергии немонохроматического электромагнитного излучения.

Если фотоэлемент последовательно освещать различными монохроматическими источниками света, имеющими в спектре испускания одну длину волны и излучающими в единицу времени одинаковую энергию, то величина фототока будет зависеть от длины волны падающего света. Поэтому наряду с понятием интегральной чувствительности фотоэлемента вводится понятие его спектральной чувствительности. Спектральная чувствительность $\gamma_\lambda$ определяется отношением силы фототока $I$ к величине падающего на фотоэлемент потока световой энергии $\Phi_\lambda$ в узком интервале длин волн от~$\lambda$~до~$\lambda+d\lambda$:
\begin{equation}
	\label{eq:06}
	\gamma_\lambda = \frac{I}{\Phi_\lambda}.
\end{equation}

Таким образом, измерив величину фототока при освещении фотоэлемента светом одинаковой интенсивности, но разной длины волны, можно было бы найти его спектральную чувствительность. Однако, на практике интенсивность монохроматических источников света с различными длинами волн неодинакова. Более того, определение спектральной чувствительности фотоэлемента связано с нахождением абсолютного значения величины потока световой энергии $\Phi_\lambda$ и является непростой задачей.

Рассмотрим метод определения относительной чувствительности фотоэлемента, который можно реализовать в учебной лаборатории.

Если белый свет лампы накаливания, испускаемый раскалённой вольфрамовой нитью, пропустить через монохроматор, то можно выделить излучение в узком интервале длин волн от~$\lambda$~до~$\lambda+d\lambda$. Согласно законам теплового излучения (Кирхгофа и Планка) поток энергии излучения вольфрамовой нити в области $d\lambda$ в окрестности длины волны $\lambda$ пропорционален следующему выражению:
\begin{equation}
	\label{eq:07}
	\Phi_\lambda \sim \varepsilon_\lambda \cdot \frac{1}{\lambda^5} \cdot \frac{1}{e^\frac{hc}{\lambda k T} - 1},
\end{equation}
где $\varepsilon_\lambda$~--- поглощательная способность вольфрама, которую можно считать постоянной в оптической области длин волн 4000--8000~\AA~(1 \AA~$= 10^{-10}$~м), $k = 1.38 \cdot 10^{-23}$~Дж/К~--- постоянная Больцмана, $T$~--- абсолютная температура вольфрамовой нити (в настоящей работе $T \approx 2900$~К).

Фототок $I$ в электрической схеме, содержащей фотоэлемент, пропорционален напряжению $U$, которое можно измерить с помощью вольтметра. Тогда с учетом формул~(\ref{eq:06})~и~(\ref{eq:07}) для спектральной чувствительности фотоэлемента можно записать:
\begin{equation}
	\label{eq:08}
	\gamma_\lambda \sim \frac{U \cdot \lambda^5 \cdot \left(e^\frac{hc}{\lambda k_B T} - 1 \right)}{\varepsilon_\lambda \cdot d\lambda}.
\end{equation}

Согласно выражению~(\ref{eq:08}), отношение спектральной чувствительности фотоэлемента $\gamma_\lambda$ для произвольной длины волны $\lambda$ к его чувствительности $\gamma_{\lambda _m}$ для фиксированной длины волны $\lambda_m$ будет равно:
\begin{equation}
	\label{eq:09}
	\frac{\gamma_\lambda}{\gamma_{\lambda _m}} = \frac{U \cdot \lambda^5 \cdot \left(e^\frac{hc}{\lambda k T} - 1 \right)}{U_m \cdot \lambda_m^5 \cdot \left(e^\frac{hc}{\lambda_m k T} - 1 \right)}.
\end{equation}

В формуле~(\ref{eq:09}) $U_m$ обозначает напряжение в цепи при освещении фотоэлемента светом с длиной волны $\lambda_m$ и считается, что в изучаемой спектральной области при постоянной величине входной щели монохроматора интервал длин волн $d\lambda$ для разных $\lambda$ изменяется незначительно. Таким образом, измеряя напряжение в цепи с фотоэлементом при его освещении светом с известными значениями длин волн, по формуле~(\ref{eq:09}) можно определить его относительную спектральную чувствительность.

\section{ПРАКТИЧЕСКАЯ ЧАСТЬ}
\subsection{Градуировка монохроматора}

%\begin{figure}[h]
\begin{wrapfigure}[16]{l}{0.45\textwidth}
\includegraphics[width=1\textwidth]{pictures/pic3.pdf}
\caption{Схема монохроматора  УМ--2.}
\label{im:04}
\end{wrapfigure}

В настоящей работе для разложения света на спектральные составляющие используется универсальный монохроматор УМ-2. Схема монохроматора показана на~рис.~\ref{im:04}.

Освещаемая светом исследуемого источника входная щель $S$ коллиматорной трубы $K$ выделяет узкий пучок света. Входная щель находится в фокусе коллиматорного ахроматического объектива $L_1$, который формирует параллельный пучок лучей. Параллельный пучок лучей падает на диспергирующий элемент~--- призму $P$, установленную на вращающемся столике $T$. Вращение столика $T$ вызывается поворотом барабана монохроматора, который расположен справа от зрительной трубы $M$.

Действие призмы основано на том, что её показатель преломления $n$ зависит от длины волны падающего света (явление дисперсии света), и, следовательно, угол отклонения луча призмой будет различным для разных длин волн.

Линза $L_2$ зрительной трубы $M$ собирает свет, разложенный призмой $T$ по длинам волн и формирует изображение спектра, который рассматривается в окуляр $O$. Окуляр имеет указатель (визир) в виде стрелки и служит для градуировки монохроматора.

Градуировкой монохроматора называют процесс, с помощью которого устанавливается однозначная связь между отсчетом по шкале делений барабана монохроматора и длиной волны спектральной линии, расположенной против указателя (визира) в зрительной трубе. Угловая дисперсия призмы существенно зависит от длины волны, поэтому градуировочные (по длинам волн) характеристики приборов нелинейны и для выполнения градуировки нужно использовать большое число линий с известными длинами волн. Для градуировки используется эталонный источник света, у которого имеются линии во всех областях спектра. Длины волн этих линий должны быть известны с высокой точностью. Результаты градуировки представляются в виде графиков, таблиц или в виде новой шкалы.

\begin{table}[H]
\caption{\label{tab:01}Градуировка монохроматора.}
\begin{center}
\begin{tabular}{| p{0.4\linewidth} | c | c |}
\hline
\centering Спектральные линии ртути & Длина волны, \AA & Показания шкалы \\
\hline
\centering Красная & 7081.9 & \\
\hline
\centering Красная & 6234.4 & \\
\hline
\centering Жёлтая & 5790.6 & \\
\hline
\centering Жёлтая & 5769.6 & \\
\hline
\centering Зелёная & 5460.7 & \\
\hline
\centering Голубая & 4916.0 & \\
\hline
\centering Синяя & 4358.3 & \\
\hline
\centering Фиолетовая & 4077.8 & \\
\hline
\centering Фиолетовая & 4046.6 & \\
\hline
\end{tabular}
\end{center}
\end{table} 

В настоящей работе в качестве эталонного источника света используется ртутная лампа. Изготовленная из специального кварцевого стекла и заполненная парами ртути трубка лампы пропускает свет в очень широком диапазоне (включая видимую и ультрафиолетовую области спектра). Трубка лампы (для защиты глаз от ультрафиолетовых лучей) помещена в светонепроницаемый корпус с небольшим окном для выхода излучения. {\bfseries Запрещается смотреть непосредственно на выходное окно ртутной лампы, т.\,к. при прямом попадании света в глаза возможен ожог сетчатки глаза.}

Включите ртутную лампу, прогрейте ее в течение 10 минут. Замените съёмную выходную щель монохроматора в зрительной трубе $M$ на окуляр $O$. Выходное окно включенной ртутной лампы расположите напротив входной щели монохроматора. С помощью конденсорной линзы сфокусируйте излучение ртутной лампы на входную щель монохроматора. Обычные стеклянные линзы и призма монохроматора задерживают ультрафиолетовое излучение, поэтому в окуляре зрительной трубы будут видны только отдельные спектральные линии различного цвета и интенсивности, принадлежащие видимому спектру ртути. Наблюдая спектр в окуляр, перемещением ртутной лампы добейтесь максимальной яркости спектральных линий. Вращение барабана монохроматора приводит к повороту столика $T$, на котором расположена призма $P$, и наблюдаемый в окуляре спектр источника излучения будет перемещаться относительно стрелки окуляра (визира). Измерения рекомендуется проводить при перемещению по спектру от красной к фиолетовым областям. Вращая барабан, рассмотрите спектр паров ртути в окуляр. Для того, чтобы увидеть две жёлтые линии ртути с близкими длинами волн, уменьшите ширину входной щели монохроматора $S$. Спектральные линии должны быть достаточно узкими. Для градуировки выбираются наиболее яркие линии ртути, точные значения длин волн которых приведены в~таблице~\ref{tab:01}. Длина волны спектральных линий в таблице приведена в ангстремах.

Совместите визир со спектральной линией ртути. Для получения наиболее точных измерений визир всегда должен приближаться к линии только с одной стороны, в нашем случае~--- с правой. Занесите показания шкалы барабана монохроматора для соответствующего цвета в~таблице~\ref{tab:01}. Проведите измерения для других линий и заполните таблицу~\ref{tab:01}. Выключите ртутную лампу.

По данным таблицы~\ref{tab:01} постройте градуировочный график (градуировочную кривую монохроматора). По оси ординат $OY$ откладываются длины волн спектральных линий ртути, по оси абсцисс $OX$~--- соответствующие им показания по шкале барабана монохроматора. Градуировочный график должен иметь вид плавной монотонной линии. С его помощью по измеренным значениям положений (делений шкалы барабана) спектральных линий любого другого излучения можно определить их длины волн.

\subsection{Определение относительной чувствительности полупроводникового фотоэлемента}
Полностью закройте входную щель монохроматора. Замените окуляр в зрительной трубе монохроматора на выходную щель. Вплотную к щели поставьте фотоэлемент с закрытой крышкой. Закройте зрительную трубу, щель и фотоэлемент светонепроницаемым кожухом из ткани. Снимите крышку фотоэлемента. Включите осветитель. С помощью конденсорной линзы добейтесь равномерного освещения входной щели монохроматора источником света. Включите в сеть подключенный к фотоэлементу микровольтметр В2-11. Установите предел показаний вольтметра~--- $30$~мВ. Приоткройте входную щель монохроматора. Медленно вращая барабан, определите показание угла $\varphi_m$ по его шкале, для которого напряжение принимает максимальное значение $U_m$. Отрегулируйте ширину входной щели в этой области таким образом, чтобы максимальное показание вольтметра не превышало значение напряжения~$30$~мВ. Величине угла $\varphi_m$ будет соответствовать длина волны $\lambda_m$, которая находится из градуировочного графика.

\begin{table}[H]
\caption{\label{tab:02}Экспериментальные данные.}
\begin{center}
\begin{tabular}{| p{0.2\linewidth} | c | c | c | c | c | c | c | c | c | c |}
\hline
\centering Величина & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 \\
\hline
$\quad \varphi$ & \phantom{miii} & \phantom{miii} & \phantom{miii} & \phantom{miii} & \phantom{miii} & \phantom{miii} & \phantom{miii} & \phantom{miii} & \phantom{miii} & \phantom{miii} \\
\hline
$\quad U$, мВ & & & & & & & & & & \\
\hline
$\quad \lambda$, м & & & & & & & & & & \\
\hline
$\quad \gamma_\lambda / \gamma_{\lambda_m}$ & & & & & & & & & & \\
\hline
\end{tabular}
\end{center}
\end{table} 

Измерьте значения напряжения $U$ для 10 различных делений шкалы барабана $\varphi$,  по обе стороны от величины $\varphi_m$, таким образом, чтобы напряжение сначала увеличивалось от минимального значения до максимальной величины, а потом снова убывало до минимального значения. Занесите эти данные в таблицу~\ref{tab:02}. По градуировочному графику определите значения длин волн $\lambda$, соответствующих делениям шкалы барабана $\varphi$. По формуле~(\ref{eq:09}) рассчитайте относительную спектральную чувствительность полупроводникового фотоэлемента ${\gamma_\lambda}/{\gamma_{\lambda _m}}$ для данных длин волн $\lambda$. Заполните таблицу~\ref{tab:02}.

По данным таблицы~\ref{tab:02} постройте график зависимости относительной чувствительности фотоэлемента ${\gamma_\lambda}/{\gamma_{\lambda _m}}$ от длины волны $\lambda$. Сделайте вывод.

\section{ПОРЯДОК ВЫПОЛНЕНИЯ РАБОТЫ}
ВНИМАНИЕ! В работе применяется ртутная лампа, являющаяся мощным источником ультрафиолетового излучения. \underline{Запрещается смотреть непосредственно на выходное окно ртутной лампы}, т.\,к. при прямом попадании света в глаза возможен ожог сетчатки глаза.

\begin{enumerate}
\item Ознакомиться с устройством монохроматора, зарисовать схему установки.
\item Заменить съёмную выходную щель в зрительной трубе монохроматора на
окуляр.
\item Включить ртутную лампу и прогреть её в течение 10 минут.
\item С помощью конденсорной линзы сфокусировать излучение ртутной лампы на входную щель монохроматора.
\item Перемещением конденсорной линзы добиться максимальной яркости спектральных линий, наблюдаемых в окуляр зрительной трубы. Чтобы увидеть раздельно две желтые линии ртути с близкими длинами волн, необходимо уменьшить ширину входной щели монохроматора. Спектральные линии должны быть достаточно узкими.
\item Измерения рекомендуется проводить при перемещению по спектру от красной к фиолетовым областям. Для получения наиболее точных измерений визир всегда должен приближаться к линии только с одной стороны, в нашем случае~--- с правой. Вращением барабана монохроматора совместить визир окуляра с красной линией ртути (для градуировки выбираются наиболее яркие спектральные линии). Записать показания шкалы барабана для данной длины волны в таблицу~\ref{tab:01}.
\item Провести измерения для других линий ртути. Заполнить таблицу~\ref{tab:01}. Выключить ртутную лампу.
\item По данным таблицы~\ref{tab:01} построить градуировочный график~--- зависимость длины волны спектральных линий ртути от показаний шкалы барабана монохроматора.
\item Полностью закрыть входную щель монохроматора. Заменить окуляр в зрительной трубе монохроматора на выходную щель. Вплотную к щели поставить фотоэлемент с закрытой крышкой. Закрыть зрительную трубу, щель и фотоэлемент светонепроницаемым кожухом из ткани. Снять крышку фотоэлемента.
\item Включить осветитель. С помощью конденсорной линзы добиться равномерного освещения входной щели монохроматора источником света.
\item Включить в сеть подключенный к фотоэлементу микровольтметр~\mbox{B2-11}.
\item Приоткрыть входную щель монохроматора. Медленно вращая барабан, определить показание угла $\varphi_m$ по его шкале, для которого напряжение принимает максимальное значение $U_m$. Отрегулировать ширину входной щели в этой области таким образом, чтобы максимальное показание вольтметра не превышало значение напряжения 30~мВ.
\item Измерить значения напряжения $U$ для 10 различных делений шкалы барабана $\varphi$, по обе стороны от величины $\varphi_m$, таким образом, чтобы напряжение сначала увеличивалось от минимального значения до максимальной величины, а потом снова убывало до минимального значения. Занести эти данные в таблицу~\ref{tab:02}.
\item Выключить осветитель.
\item По градуировочному графику найти длины волн $\lambda$, соответствующие делениям шкалы барабана $\varphi$.
\item По формуле~(\ref{eq:09}) рассчитать относительную спектральную чувствительность полупроводникового фотоэлемента для этих длин волн (при вычислениях длины волн $\lambda$ необходимо выразить в метрах). Заполнить таблицу~\ref{tab:02}.
\item По данным таблицу~\ref{tab:02} построить график зависимости относительной чувствительности фотоэлемента от длины волны $\lambda$.
\item Сделать вывод и оформить отчёт.
\end{enumerate}

\section{КОНТРОЛЬНЫЕ ВОПРОСЫ}
\begin{enumerate}
\item В чем заключается явление внешнего фотоэффекта?
\item Сформулируйте и объясните законы внешнего фотоэффекта.
\item Чем внутренний фотоэффект отличается от внешнего?
\item Опишите принцип действия полупроводникового вентильного фотоэлемента.
\item Дайте определение интегральной и спектральной чувствительности фотоэлемента.
\item Как находится поток световой энергии?
\item Напишите формулу для нахождения относительной спектральной чувствительности полупроводникового фотоэлемента и поясните ее.
\item Для чего в работе используется монохроматор?
\item Как проводится градуировка монохроматора?
\item Как строится и используется градуировочный график?
\end{enumerate}
