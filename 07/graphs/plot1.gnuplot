#! /usr/bin/gnuplot -persist
set terminal epslatex
set output "plot1.tex"
set encoding utf8
#set xlabel font "Libertine, 16"
#set ylabel font "Libertine, 16"
#set xlabel "\\Large $U$"
#set ylabel "\\Large $I$"
set label font "Libertine, 16"
set key off
#set format y "%2.1e"

set xrange [-7:10]
set yrange [-2:350]

set arrow from -1, graph 0 to -1, graph 1 #yaxis
set arrow from -7, graph 0 to 10, graph 0 #xaxis
set arrow from -1.2, 290 to 10, 290 nohead lt 2 lw 3#assymptote
set arrow from -5.9, 3 to -5.9, -6 nohead #Uz
#set grid x y
unset border
#set yzeroaxis lt 1 lw 3
unset xtics
unset ytics
#set ytics ("\\LARGE $I_{\\textsc{н}}$" 300)


set label "\\LARGE $I$" at -1.5, graph 1 center #Ylabel
set label "\\LARGE $I_{\\textsc{н}}$" at -1.7, 300 center #In
set label "\\LARGE $U$" at 9.2, graph -0.06 #Xlabel
set label "\\LARGE $U_{\\textsc{з}}$" at -6.3, graph -0.06 #Uz
set label "\\LARGE $0$" at -1, graph -0.05 #Zero point

phi1(x) = (x<0)?exp(0.54*x+5)-8:289-exp(-0.54*x+5)

plot phi1(x) lw 4