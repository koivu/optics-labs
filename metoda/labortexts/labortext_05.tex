\textit{Целью работы} является изучение явления дисперсии света; определение зависимости показателя преломления от длины волны, дисперсии вещества и угловой дисперсии призмы.

\textit{Обеспечивающие средства:} ртутная лампа, коллиматор, зрительная труба, гониометр, призмы из стекла <<флинт>> и <<крон>>.

\section{ТЕОРЕТИЧЕСКОЕ ВВЕДЕНИЕ}
\subsection{Электронная теория дисперсии}

\begin{wrapfigure}[9]{l}{0.45\linewidth}
\includegraphics[width=1\linewidth]{05/pic1.pdf}
\caption{Разложение белого света в спектр.}
\label{img:whitelightSpectrum_05}
\end{wrapfigure}

Разложение белого света в спектр с помощью стеклянной призмы (рис.~\ref{img:whitelightSpectrum_05}) впервые было изучено И. Ньютоном. Его опыты, а также эксперименты предшественников показали, что фиолетовые лучи отклоняются от первоначального направления сильнее, чем красные. Это явление можно объяснить, если предположить, что стекло, из которого сделана призма, имеет различный показатель преломления для различных длин волн. Тогда лучи с разными длинами волн $\lambda$ будут распространяться в призме по различным направлениям и выйдут из призмы тоже под разными углами -- произойдёт разложение падающего излучения в спектр.

Дисперсией света называются явления, обусловленные зависимостью показателя преломления от частоты или длины волны излучения:
\begin{equation}\label{eq:orpedelDisp_05}
n=n(\lambda).
\end{equation}

Один из важнейших выводов электромагнитной теории света Максвелла состоит в том, что показатель преломления электромагнитных волн равен (в системе СГСЭ):
\begin{equation}\label{eq:pokazatPreloml_05}
n=\sqrt{\varepsilon \, \mu}.
\end{equation}

Здесь $\varepsilon$ и $\mu$~--- диэлектрическая и магнитная проницаемости среды -- постоянные, которые в первоначальной теории полагались не зависящими от частоты падающего света. Поскольку для большинства диэлектриков с высокой точностью $\mu=1$, то показатель преломления:
\begin{equation}\label{eq:pokazatPrelomlDielectr_05}
n=\sqrt{\varepsilon}, \qquad \text{или} \qquad n^2=\varepsilon.
\end{equation}

Феноменологическая электромагнитная теория света Максвелла не объясняла и не может объяснить теорию дисперсии. Трудности объяснения дисперсии света с точки зрения электромагнитной теории устраняются с помощью электронной теории.

Классическая теория дисперсии, предложенная впервые Г.\,А.~Лорентцем, основана на воздействии светового поля (электромагнитных волн) на связанные электроны атомов с учётом их торможения. Согласно электронной теории дисперсии, диэлектрик рассматривается как совокупность осцилляторов, совершающих вынужденные колебания под действием светового излучения.

Для того, чтобы получить соотношение, связывающее показатель преломления с длиной волны, необходимо вначале найти, как зависит от частоты диэлектрическая проницаемость $\varepsilon$, а затем на основании формулы~(\ref{eq:pokazatPrelomlDielectr_05}) перейти к показателю преломления.

Рассмотрим поведение прозрачного изотропного вещества в электромагнитном поле световой волны. Пусть в единице объёма вещества содержится $N$ атомов~-- осцилляторов. Для простоты будем предполагать, что:
\begin{enumerate}
\item среда состоит из одного сорта атомов;
\item каждый атом содержит только один электрон, взаимодействующий со световой волной;
\item электроны соседних атомов не взаимодействуют друг с другом.
\end{enumerate}

Рассмотрим поляризацию среды под действием внешнего электромагнитного поля. Согласно электронной теории электроны в атомах диэлектрика находятся в состоянии равновесия. Под действием внешнего поля они смещаются от положения равновесия на некоторое расстояние $r$, превращая атом в электрический диполь с дипольным моментом $\V{p}=e \cdot \V{r}$. При этом электрический момент единицы объёма (поляризованность среды) равен:
\begin{equation}\label{eq:elMoment_05}
\V{P}=N \cdot \V{p} = N \cdot e \cdot \V{r},
\end{equation}
здесь $e$~--- заряд электрона.

Связь между векторами электрической индукции $\V{D}$, напряжённостью электрического поля внутри диэлектрика $\V{E}$ и поляризованностью среды $\V{P}$ имеет вид:
\begin{equation}\label{eq:relationDEP_05}
\V{D}= \varepsilon \V{E} = 
\V{E} + 4 \pi \V{P}.
\end{equation}

Подставляя формулу~(\ref{eq:elMoment_05}) в выражение~(\ref{eq:relationDEP_05}):
\begin{equation}\label{eq:relationDEPupdate1_05}
\varepsilon \V{E} = 
\V{E} + 4 \pi N e \V{r},
\end{equation}
или с учётом формулы~(\ref{eq:pokazatPrelomlDielectr_05}), в проекции на направление радиус-вектора имеем:
\begin{equation}\label{eq:relationDEPupdate2_05}
n^2 = \varepsilon = 
1 + \cfrac{4 \pi N e r}{E}.
\end{equation}

Рассмотрим силы, действующие на электрон в атоме, при действии на него электромагнитной волны.

\textit{Вынуждающая сила.} Вынужденные колебания электрона возникают под действием световой волны, распространяющейся в среде. В этом случае на электрон действует вынуждающая сила:
\begin{equation}\label{eq:ForceVinuzd_05}
\V{F}_{\text{вын}} = e \cdot \V{E} + \cfrac{e}{c} \left[ \cfrac{d\V{r}}{dt} \times \V{H} \right],
\end{equation}
где $\V{E}$ и $\V{H}$~--- напряжённости электрического и магнитного поля световой волны, соответственно; $c$~--- скорость света в вакууме; $\cfrac{d\V{r}}{dt}= \boldsymbol{\upsilon} $~--- скорость движения электрона в атоме.

Так как второе слагаемое в формуле~(\ref{eq:ForceVinuzd_05}) в $\upsilon/c \approx 10^{-3}$ раз меньше, чем первое, то магнитная составляющая поля оказывает очень малое воздействие на движущийся электрон. Поэтому этим слагаемым можно пренебречь. В~этом случае выражение для вынуждающей силы принимает вид:
\begin{equation}\label{eq:ForceVinuzdUpdate1_05}
\V{F}_{\text{вын}} = e \cdot \V{E}.
\end{equation}

Будем исходить из того, что напряжённость электрического поля световой волны изменяется по гармоническому закону:
\begin{equation}\label{eq:Egarmonic_05}
\V{E} = \V{E}_0\exp(i\omega t),
\end{equation}
где $\omega$~--- круговая частота падающего излучения.

\textit{Удерживающая сила.} Представляя атом гармоническим осциллятором с определенной круговой частотой собственных колебаний $\omega_0$, можно считать, что электрон в атоме удерживается в положении равновесия квазиупругой силой:
\begin{equation}\label{eq:ForceUprug_05}
\V{F}_{\text{упр}} = - k \V{r},
\end{equation}
которая пропорциональна смещению электрона от положения равновесия~$r$, возникающему под действием поля световой волны. Квазиупругая сила всегда имеет знак обратный направлению смещения. 

Под действием квазиупругой силы электрон совершает свободные колебания с собственной частотой~$\omega_0$, определяемой только его массой~$m$ и коэффициентом квазиупругой связи $k$:
\begin{equation}\label{eq:relationOmegaMK_05}
\omega_0 = \sqrt{\cfrac{k}{m}}.
\end{equation}

\textit{Тормозящая сила.} Допущение о гармоническом колебании электрона в атоме имеет приближенный характер. В действительности колеблющийся электрон в атоме испускает электромагнитные волны и постепенно теряет свою энергию и, следовательно, амплитуда его колебаний с течением времени уменьшается, т.\,е. происходит процесс затухания.

Потеря энергии электроном связана не только с излучением, но и с взаимодействием атомов среды между собой (например, столкновениями), с хаотическим тепловым движением атомов (эффект Доплера) и т.\,п. Феноменологически потерю энергии осциллирующим электроном можно учесть введением силы сопротивления (трения), пропорциональной скорости электрона, как это делается в механике:
\begin{equation}\label{eq:ForceSoprotivl_05}
\V{F}_{\text{сопр}} = - g \cfrac{d\V{r}}{dt},
\end{equation}
где $g$~--- коэффициент, зависящий от природы атома. Отрицательный знак в выражении для силы сопротивления указывает на то, что она всегда направлена против смещения электрона от положения равновесия.

Таким образом, дифференциальное уравнение движения электрона в атоме будет иметь вид:
\begin{equation}\label{eq:difEqFull_05}
m \cfrac{d^2 \V{r}}{dt^2}=
\V{F}_{\text{вын}} +
\V{F}_{\text{упр}} +
\V{F}_{\text{сопр}},
\end{equation}
или с учётом формул~(\ref{eq:ForceVinuzdUpdate1_05}), (\ref{eq:ForceUprug_05}), (\ref{eq:ForceSoprotivl_05}):
\begin{equation}\label{eq:difEqFullUpdate1_05}
m \cfrac{d^2 \V{r}}{dt^2}=
e \cdot \V{E} -
k \V{r} -
g \cfrac{d\V{r}}{dt}.
\end{equation}

Разделим обе части уравнения~(\ref{eq:difEqFullUpdate1_05}) на массу электрона $m$. Введём обозначения: $\gamma = \cfrac{g}{m}$, $\omega_0^2 = \cfrac{k}{m}$ и перегруппируем слагаемые, получим:
\begin{equation}\label{eq:difEqFullUpdate2_05}
\cfrac{d^2 \V{r}}{dt^2} +
\gamma \cfrac{d\V{r}}{dt}+
\omega_0^2 \V{r} =
\cfrac{e}{m}\,\V{E}.
\end{equation}

Нас интересует частное решение уравнения~(\ref{eq:difEqFullUpdate2_05}), которое описывает установившиеся вынужденные колебания электрона в атоме. Эти колебания под действием гармонической внешней силы~(\ref{eq:ForceVinuzdUpdate1_05}) с учётом~(\ref{eq:Egarmonic_05}) также будут гармоническими, и их частота совпадает с частотой вынуждающей силы $\omega$. Поэтому решение уравнения~(\ref{eq:difEqFullUpdate2_05}) для смещения электрона от положения равновесия можно искать в виде:
\begin{equation}\label{eq:solveDifEq_05}
\V{r}=\V{r}_0\cdot \exp(i \omega t).
\end{equation}

Замечая, что: $\cfrac{d\V{r}}{dt}=i\omega \V{r}$, $\cfrac{d^2\V{r}}{dt^2}=\left(i\omega\right)^2 \! \V{r}$, из уравнения~(\ref{eq:difEqFullUpdate2_05}) следует:
\begin{equation}\label{eq:difEqFullUpdate3_05}
\V{r}(-\omega^2+i\gamma \omega + \omega_0^2)=
\cfrac{e}{m}\,\V{E},
\end{equation}
или
\begin{equation}\label{eq:difEqFullUpdate4_05}
\V{r}=\cfrac{e}{m((\omega_0^2 - \omega^2)+i\gamma \omega)}\, \V{E}.
\end{equation}

Подставляя выражения~(\ref{eq:solveDifEq_05}) и~(\ref{eq:difEqFullUpdate4_05}) в формулу~(\ref{eq:relationDEPupdate2_05}), получаем зависимость показателя преломления среды от частоты падающего света, т.\,е. существование дисперсии:
\begin{equation}\label{eq:disperSootn_05}
n^2 = \varepsilon = 1 +
\cfrac{4\pi N e^2}{m((\omega_0^2-\omega^2)+i\gamma \omega)}.
\end{equation}

Согласно выражению~(\ref{eq:disperSootn_05}), диэлектрическая проницаемость $\varepsilon$ (a следовательно, и показатель преломления) -- величина комплексная. Если в~(\ref{eq:disperSootn_05}) положить $\gamma=0$, то диэлектрическая проницаемость будет вещественной. Переход от комплексного значения показателя преломления к вещественному означает пренебрежение поглощением электромагнитной волны. Рассмотрим это приближение:
\begin{equation}\label{eq:priblizhenie_05}
n^2 = \varepsilon = 1 +
\cfrac{4\pi N e^2}{m(\omega_0^2-\omega^2)}.
\end{equation}

Можно выявить те условия, при которых приближение $\gamma = 0$ имеет вполне определенный физический смысл. Все прозрачные тела не имеют полос поглощения в видимой области спектра, а при переходе в ультрафиолетовую область подавляющее большинство таких тел начинает интенсивно поглощать электромагнитные волны (здесь не учитываются инфракрасные полосы поглощения, наблюдаемые у некоторых прозрачных тел). Для всей видимой части спектра справедливо неравенство $\omega \ll \omega_0$, т.\,е. дисперсия изучается вдали от линий поглощения. Иными словами, частота собственных колебаний осциллирующего электрона соответствует ультрафиолетовой области спектра.

Используя предположение $\omega \ll \omega_0$, можно разложить выражение~(\ref{eq:priblizhenie_05}) в ряд по степеням $\omega/\omega_0$ и ограничиться в этом разложении двумя членами:
\begin{equation}\label{eq:priblizhenieUpdate1_05}
n^2 = \varepsilon = 1 +
\cfrac{4\pi N e^2}{m\omega_0^2} \left[ 1 + \left( \cfrac{\omega}{\omega_0}\right)^2 \right].
\end{equation}

В~(\ref{eq:priblizhenieUpdate1_05}) заменим $\omega=\cfrac{2\pi c}{\lambda}$ и $\omega_0=\cfrac{2\pi c}{\lambda_0}$, где $\lambda$ и $\lambda_0$~--- длина волны падающего излучения и длина волны, соответствующая собственным колебаниям электрона в атоме. Следовательно:
\begin{equation}\label{eq:priblizhenieUpdate2_05}
n^2 = 1 +
A \left( 1 + \cfrac{B}{\lambda^2}\right) ,
\end{equation}
где $A=\cfrac{4\pi N e^2}{m \omega_0^2}$, $B=\cfrac{4\pi^2 c^2}{\omega_0^2}$ и отношение $\cfrac{B}{A}$ не зависит от частоты собственных колебаний электрона $\omega_0$. Константы $A$ и $B$ в выражении~(\ref{eq:priblizhenieUpdate2_05}) можно оценить из опыта, например, из наблюдения зависимости $n(\lambda)$.

Соотношение~(\ref{eq:priblizhenieUpdate2_05}) согласуется с известной формулой Коши:
\begin{equation}\label{eq:formKoshi_05}
n = A + \cfrac{B}{\lambda^2} + \cfrac{C}{\lambda^4}+\ldots ,
\end{equation}
которая получена из представлений теорий упругости и хорошо описывает экспериментальную зависимость показателя преломления от длины волны для прозрачных веществ.

При больших концентрациях вещества необходимо учитывать взаимодействие между электронами атомов. В этом случае формула~(\ref{eq:priblizhenie_05}) принимает вид:
\begin{equation}\label{eq:priblizhenieForConcentr_05}
\cfrac{n^2-1}{n^2+2} =
\cfrac{4\pi}{3} N \cfrac{e^2}{m(\omega_0^2-\omega^2)}.
\end{equation}

\subsection{Призменные спектральные приборы, дисперсия призмы}
В основе действия спектральных приборов, предназначенных для разложения света сложного состава на составляющие и для пространственного разделения по длинам волн, лежат такие явления, как интерференция, дифракция и дисперсия света.

Спектральный прибор, диспергирующим элементом которого является призма, называется призменным спектроскопом (если картина наблюдается визуально) или спектрографом (если спектр фотографируется или записывается при помощи специального устройства).

\begin{wrapfigure}[14]{l}{0.45\linewidth}
\includegraphics[width=1\linewidth]{05/pic2.pdf}
\caption{Зависимость показателя преломления от длины волны проходящего светового пучка.}
\label{img:relationNLambda_05}
\end{wrapfigure}

Говоря о дисперсии, следует различать дисперсию материала, из которого сделан диспергирующий элемент, и дисперсию самого прибора.

Показатель преломления прозрачного материала (в частности, стекла) зависит от длины волны проходящего светового пучка (примерный вид такой зависимости приведен на рис.~\ref{img:relationNLambda_05}). Эта зависимость будет различна как для разных материалов, так и для одного и того же материала в различных участках спектра.

Показатель преломления изменяется быстрее вблизи полосы поглощения. Скорость его изменения при изменении длины волны называется дисперсией материала. Дисперсия материала численно равна~ $dn/d\lambda$, где $dn$ изменение показателя преломления при изменении длины волны на величину $d\lambda$. Дисперсия прибора характеризует скорость изменения угла отклонения светового пучка в приборе при изменении длины волны. Такая дисперсия называется угловой дисперсией прибора и определяется отношением:
\begin{equation}\label{eq:uglovDisper_05}
D_\theta = \cfrac{d\theta}{d\lambda},
\end{equation}
где $d\theta$~--- угол между лучами с длинами волн $\lambda$ и $\lambda + d\lambda$ (рис.~\ref{img:geometryOfProblem_05}).

\begin{wrapfigure}[9]{l}{0.47\linewidth}
\includegraphics[width=1\linewidth]{05/pic3.pdf}
\caption{Геометрия задачи.}
\label{img:geometryOfProblem_05}
\end{wrapfigure}

В спектральном приборе призма устанавливается симметричным образом так, что в самой призме луч света распространяется параллельно основанию (на рис.~\ref{img:geometryOfProblem_05} это луч с длиной волны~$\lambda$). В этом случае угол $\theta$, характеризующий отклонения луча после преломления его в призме, минимален (т.\,е. призма установлена под углом наименьшего отклонения) и внутри призмы луч идет параллельно ее основанию. Если обозначить через $A$ преломляющий угол призмы при её вершине, в случае угла наименьшего отклонения $\theta$ можно получить формулу для показателя преломления материала призмы:
\begin{equation}\label{eq:nForPrizm_05}
n=\cfrac{\sin \left( \cfrac{A+\theta}{2} \right)}{\sin \left( \cfrac{A}{2}\right)}.
\end{equation}

При учёте зависимости показателя преломления материала призмы (стекла) от длины волны получается следующее выражение для угловой дисперсии призмы:
\begin{equation}\label{eq:uglovDisperUpdate_05}
D_\theta = \cfrac{d\theta}{d\lambda} =
\cfrac{d\theta}{dn} \cdot \cfrac{dn}{d\lambda}.
\end{equation}

Продифференцируем выражение~(\ref{eq:nForPrizm_05}):
\begin{equation}\label{eq:dnDtheta_05}
\cfrac{dn}{d\theta}=\cfrac{1}{2}\cdot
\cfrac{\cos \left( \cfrac{A+\theta}{2}\right)}{\sin \left( \cfrac{A}{2}\right)}.
\end{equation}

Согласно основному тригонометрическому тождеству из~(\ref{eq:nForPrizm_05}) следует:
\begin{equation}\label{eq:overTrigonometric_05}
\cos \left( \cfrac{A+\theta}{2}\right) =
\sqrt{1-\sin^2 \left( \cfrac{A+\theta}{2}\right)}=
\sqrt{1-n^2\sin^2 \left( \cfrac{A}{2}\right)}.
\end{equation}

Выражение для угловой дисперсии призмы~(\ref{eq:uglovDisperUpdate_05}) с учётом~(\ref{eq:dnDtheta_05}) и~(\ref{eq:overTrigonometric_05}) принимает вид:
\begin{equation}\label{eq:uglovDisperFinal_05}
D_\theta =
\cfrac{2\sin\left( \cfrac{A}{2}\right)}{\sqrt{1-n^2\sin^2 \left( \cfrac{A}{2}\right)}} \cdot \cfrac{dn}{d\lambda}.
\end{equation}

Таким образом, при заданной геометрии (угол $A$ обычно составляет примерно $60^{\circ}$, так как при больших углах для некоторых длин волн уже наступает полное внутреннее отражение на второй грани призмы) дисперсия призмы целиком определяется значениями $n$ и $\cfrac{dn}{d\lambda}$. Очевидно, что выгодно использовать оптические материалы с большими значениями этих величин. Так как для всех прозрачных веществ (рис.~\ref{img:geometryOfProblem_05}) показатель преломления увеличивается с уменьшением длины волны (т.\,е. имеет место нормальная дисперсия), то использование призмы в качестве диспергирующего элемента наиболее выгодно именно в коротковолновой области. Правда тяжелые сорта стекла (<<флинт>>) с наибольшими значениями показателя преломления очень сильно поглощают фиолетовые лучи и для исследования в этой пограничной с ультрафиолетом области часто используют более прозрачное легкое стекло (<<крон>>), у которого $n$ и $\cfrac{dn}{d\lambda}$ значительно меньше, чем у <<флинта>>.

\section{ПРАКТИЧЕСКАЯ ЧАСТЬ}

\subsection{Определение показателя преломления стекла, дисперсии вещества и угловой дисперсии призмы}

Схема экспериментальной установки приведена на рис.~\ref{img:geometryOfExperiment_05}. Здесь: $S$~--- входная щель; $K$~--- коллиматор; $F$~--- зрительная труба. Коллиматор состоит из объектива и щели $S$, закреплённой на патрубке. 

Щель $S$ освещается светом от источника излучения. Щель помещают в фокальную плоскость объектива коллиматора, поэтому свет от каждой точки щели выходит из коллиматора параллельным пучком по направлению прямой, соединяющей эту точку с центром линзы объектива (рис.~\ref{img:geometryOfExperiment_05}).

\begin{figure}[H]
\includegraphics[width=0.75\linewidth]{05/pic4.pdf}
\caption{Схема экспериментальной установки.}
\label{img:geometryOfExperiment_05}
\end{figure}

Далее этот параллельный пучок лучей падает на диспергирующий элемент~--- призму, которая установлена на вращающемся столике. Проходя через призму, лучи света дважды преломляются, в результате чего отклоняются от своего первоначального направления. Вследствие дисперсии~--- зависимости показателя преломления материала призмы от длины волны падающего излучения, свет сложного спектрального состава разлагается призмой на несколько идущих по разным направлениям лучей с различными длинами волн. При этом лучи с меньшей длиной волны (фиолетовые) отклоняются призмой от своего первоначального направления сильнее, чем лучи с большей длиной волны (красные).

Зрительная труба $F$, в состав которой входит объектив и окуляр, предназначена для регистрации угла, на который отклоняется пучок света. Попадая в зрительную трубу, параллельные пучки лучей собираются в фокальной плоскости объектива зрительной трубы, образуя изображения $S'$ точек щели. Получившееся изображение щели рассматривают через окуляр зрительной трубы. Измерение угла, на который отклоняется пучок света, производится с помощью шкалы градусов гониометра, на котором расположен вращающийся столик с призмой.

В настоящей работе в качестве источника света используется ртутная лампа с известным спектром испускания. Изготовленная из специального кварцевого стекла и заполненная парами ртути трубка лампы пропускает свет в очень широком диапазоне (включая видимую и ультрафиолетовую области спектра). Трубка лампы (для защиты глаз от ультрафиолетовых лучей) помещена в светонепроницаемый корпус с небольшим окном для выхода излучения. \textbf{Запрещается смотреть непосредственно на выходное окно ртутной лампы}, т.\,к. при прямом попадании света в глаза возможен ожог сетчатки глаза.

Включите ртутную лампу в сеть. Лампа должна прогреться в течение 10 минут. Выходное окно включенной ртутной лампы необходимо расположить напротив входной щели коллиматорной трубы. Установите объектив зрительной трубы напротив объектива коллиматора по прямолинейному ходу лучей света. В окуляре зрительной трубы должно наблюдаться белое изображение входной щели коллиматора. Перемещая окуляр вдоль зрительной трубы, получите наиболее чёткое и узкое изображение щели. Совместите продольную чёрную нить~--- визир окуляра с изображением щели. Пользуясь шкалой градусов гониометра, запишите показания угла нулевого отсчёта~$\varphi_0$ с точностью до минут. Проведите измерения нулевого угла три раза. Усредните полученные значения ($1^{\circ}$ равен $60'$ минутам).

Установите на вращающийся столик гониометра призму из стекла <<флинт>>. \textbf{Внимание! Нельзя брать призму за боковые грани!} Вращая столик, добейтесь того, чтобы свет лампы, проходящий через коллиматор, попадал на середину боковой грани призмы под некоторым углом (рис.~\ref{img:geometryOfProblem_05}). Перемещая зрительную трубу к основанию призмы и при необходимости вращая столик, на котором стоит призма, рассмотреть в окуляр спектр испускания ртути: жёлтую, зелёную, сине-зелёную, синюю и две фиолетовых спектральные линии.

Сфокусируйте окуляр на изображение жёлтой спектральной линии, перемещая его вдоль зрительной трубы (линия должна быть чёткой и узкой). Совместите визир окуляра с изображением жёлтой линии. Вращая сначала столик, а потом и зрительную трубу, удерживайте линию с наведенным визиром в поле зрения. При этом зрительную трубу необходимо вращать так, чтобы угол между ней и коллиматором уменьшался, что соответствует уменьшению угла отклонения лучей призмой. В некотором положении столика его дальнейший поворот начнёт приводить не к уменьшению, а к увеличению угла отклонения лучей призмой. Линия <<остановится>> и начнёт возвращаться назад, в сторону обратного поворота зрительной трубы. Это положение как раз соответствует установке призмы на угол наименьшего отклонения для выбранной длины волны в спектре источника. Вращением зрительной трубы совместите визир окуляра с наблюдаемой линией. После этого, вращая столик с призмой, убедитесь, соответствует ли установка призмы углу наименьшего отклонения. Если окажется, что при вращении призмы линия немного сошла с положения визира в сторону уменьшения отклонения, то необходимо откорректировать зрительную трубу и навести визир на линию.

Пользуясь шкалой градусов, запишите показания угла отклонения~$\varphi$ с точностью до минут. Проведите измерения три раза. Усредните полученные значения. Занесите эти данные в таблицу~\ref{table1_05}.

\begin{table}[H]
\includegraphics[width=1\linewidth]{05/tbl1.pdf}
\caption{}
\label{table1_05}
\end{table}

%\begin{table}[H]
%\begin{tabular}{|m{2.8cm}|c|c|c|c|c|c|}
%\hline 
%\multirow{2}{2.8cm}{{\small Спектральные линии  ртути}} &\multicolumn{3}{c|}{{\small Призма из стекла <<флинт>>}} & \multicolumn{3}{c|}{{\small Призма из стекла <<крон>>}} \\ 
%\cline{2-7}
%& {\small $\varphi_1, \varphi_2, \varphi_3$} & {\small $\overline{\varphi}$} & {\small $\theta = \overline{\varphi} - \overline{\varphi_\text{0}}$} & {\small $\varphi_1, \varphi_2, \varphi_3$} & {\small $\overline{\varphi}$} & {\small $\theta = \overline{\varphi} - \overline{\varphi_\text{0}}$}\\
%\hline
%\multicolumn{1}{|c|}{\small жёлтая} & & & & & & \\
%\hline
%\multicolumn{1}{|c|}{\small зелёная} & & & & & & \\ 
%\hline
%\multicolumn{1}{|c|}{\small синяя} & & & & & & \\ 
%\hline
%\multicolumn{1}{|c|}{\small фиолетовая (слабая)} & & & & --- & --- & --- \\ 
%\hline
%\multicolumn{1}{|c|}{\small фиолетовая (яркая)}& & & & & & \\ \hline
%\end{tabular}
%\caption{}\label{table1_05}
%\end{table}

Проведите измерения углов наименьшего отклонения для остальных линий ртути, каждый раз фокусируя окуляр на исследуемую спектральную линию.

Замените призму из стекла <<флинт>> на призму из стекла <<крон>>. Повторите измерения и занесите их в соответствующие столбцы таблицы~\ref{table1_05}.

Уберите призмы в коробки и выключите ртутную лампу.

Рассчитайте углы наименьшего отклонения $\theta = \overline{\varphi} - \overline{\varphi}_0$, где $\overline{\varphi}_0$~--- среднее значение угла нулевого отсчёта, $\overline{\varphi}$~--- среднее значение измеренного угла отклонения. Заполните таблицу~\ref{table1_05}.

По формуле~(\ref{eq:nForPrizm_05}) рассчитайте показатель преломления для каждой длины волны для обеих призм с точность до третьего знака после запятой. Преломляющий угол призмы $A$ равен $60^{\circ}$. Занесите эти данные в таблицу~\ref{table2_05}. Длина волны спектральных линий ртути в таблице приведена в ангстремах (1 \AA{} = $10^{-8}$  см).

Рассчитайте значения $1/\lambda^2$. Постройте графики зависимости~$n$ от~$1/\lambda^2$ для обеих призм. Сравните полученные кривые с ожидаемым расчётом по формуле~(\ref{eq:formKoshi_05}), сделайте вывод.

\begin{table}[H]
\includegraphics[width=1\linewidth]{05/tbl2.pdf}
\caption{}
\label{table2_05}
\end{table}

%\begin{table}[H]
%%\begin{tabular}{|m{1.5cm}|c|m{1cm}|c|m{1.9cm}|m{1cm}|c|m{1.9cm}|}
%\begin{tabular}{|p{1.9cm}|c|c|c|c|c|c|c|}
%\hline
%\multirow{2}{1.5cm}{{\small $\lambda$, \AA{}}} & 
%\multirow{2}*{{\small $1/\lambda^2$, $\text{\AA{}}\!^{-2}$}} & 
%\multicolumn{3}{p{2.7cm}|}{\centering{\small Призма из стекла \newline <<флинт>>}} & 
%\multicolumn{3}{p{2.7cm}|}{\centering{\small Призма из стекла \newline <<крон>>}} \\
%\cline{3-8}
%& & {\small $n$} & 
%{\small $dn/d\lambda$} &
%{\small $D_{\theta}$, рад/\AA{}}& 
%{\small $n$} & {\small $dn/d\lambda$} & 
%{\small $D_{\theta}$, рад/\AA{}} \\ 
%\hline
%{\small 5780 \newline (жёлтая)} & & & & & & &\\ 
%\hline
%{\small 5461 \newline (зелёная)}& & & & & & &\\ 
%\hline
%{\small 4916 \newline (сине-зелёная)}& & & & & & &\\ 
%\hline
%{\small 4358 \newline (синяя)}& & & & & & &\\ 
%\hline
%{\small 4078 \newline (фиолетовая)}& & & & & & &\\ 
%\hline
%{\small 4047 \newline (фиолетовая)}& & & & & & &\\ 
%\hline
%\end{tabular}
%\caption{}\label{table2_05}
%\end{table}

Постройте графики зависимости $n$ от $\lambda$ для обеих призм. Найдите дисперсию материала призм $\cfrac{dn}{d\lambda}$. Это можно сделать графически, проводя касательные в точках кривой, соответствующих известным значениям $\lambda$, и определяя тангенс угла наклона этих касательных по изменению показателя преломления $dn$ при изменении длины волны на величину $d\lambda$. Для аналитического определения $\cfrac{dn}{d\lambda}$, построй графики зависимости $n$ от $\lambda$ в редакторе электронных таблиц (используйте точечную диаграмму). Добавьте к построенным диаграммам линию тренда, используйте полином второй степени. В качестве параметров укажите показать уравнение линии тренда и квадрат коэффициента корреляции на диаграмме (чем последний ближе к единице, тем достовернее аппроксимация экспериментальных данных линией тренда). Продифференцируйте полученное уравнение по переменной $x$ (в данном уравнении роль $x$ играет длина волны $\lambda$). Рассчитайте производную в точках $\lambda$, соответствующих известным значениям длин волн. Эти значения будут равны $\cfrac{dn}{d\lambda}$. Внесите эти данные в таблицу~\ref{table2_05}. По формуле~(\ref{eq:uglovDisperFinal_05}) рассчитайте угловую дисперсию $D_\theta$ в $\cfrac{\text{рад}}{\text{\AA{}}}$ обеих призм для всех длин волн. Сделайте вывод и оформите отчёт.

\section{ПОРЯДОК ВЫПОЛНЕНИЯ РАБОТЫ}
\textbf{ВНИМАНИЕ!} В работе применяется ртутная лампа, являющаяся мощным источником ультрафиолетового излучения. \textbf{Запрещается смотреть непосредственно на выходное окно ртутной лампы}, т.\,к. при прямом попадании света в глаза возможен ожог сетчатки глаза. \textbf{Нельзя брать призму за боковые грани!}

\begin{enumerate}
\item Ознакомиться с экспериментальной установкой.
\item Включить ртутную лампу в сеть и прогреть ее в течение 10 минут.
\item Выходное окно включенной ртутной лампы расположить напротив входной щели коллиматорной трубы. Установить объектив зрительной трубы напротив объектива коллиматора по прямолинейному ходу лучей света. В окуляре зрительной трубы должно наблюдаться белое изображение входной щели коллиматора.
\item Переместить окуляр вдоль зрительной трубы и получить наиболее чёткое и узкое  изображение щели.  Совместить продольную черную нить~--- визир окуляра с изображением щели.
\item Пользуясь шкалой градусов гониометра, записать показания угла нулевого отсчёта $\varphi_0$ с точностью до минут. Провести измерения нулевого угла три раза. Усреднить полученные значения ($1^{\circ}$ равен~$60'$).
\item Установить на вращающийся столик гониометра призму из стекла <<флинт>>. \textbf{Нельзя брать призму за боковые грани!} Повернуть столик так, чтобы свет
лампы, проходящий через коллиматор, попадал на середину боковой грани призмы под некоторым углом (рис.~\ref{img:geometryOfProblem_05}).
\item Переместить зрительную трубу к основанию призмы и при необходимости вращая  столик,  на котором  стоит призма, рассмотреть в окуляр спектр испускания ртути: жёлтую, зелёную, сине-зелёную, синюю и две фиолетовых спектральные линии.
\item Сфокусировать окуляр на изображение жёлтой спектральной линии, перемещая его вдоль зрительной трубы (линия должна быть чёткой и узкой). Совместите визир окуляра с изображением жёлтой линии.
\item Вращая сначала столик, а потом и зрительную трубу, удерживайте линию с наведенным визиром в поле зрения. При этом зрительную трубу необходимо вращать  так,  чтобы  угол  между  ней и коллиматором  уменьшался,  что соответствует уменьшению угла отклонения лучей призмой. В некотором положении   столика   его   дальнейший   поворот   начнёт   приводить   не   к уменьшению,   а  к  увеличению  угла отклонения  лучей   призмой. Линия <<остановится>> и начнёт возвращаться назад, в сторону обратного поворота зрительной трубы. Это положение как раз соответствует установке призмы на угол   наименьшего   отклонения   для  выбранной  длины   волны   в   спектре источника.
\item Вращением зрительной трубы совместить визир окуляра с наблюдаемой линией. После этого, вращая столик с призмой, убедиться, что установка призмы соответствует углу наименьшего отклонения. Если окажется, что при вращении призмы линия немного сошла с положения визира в сторону уменьшения отклонения, то необходимо откорректировать зрительную трубу и навести визир на линию.
\item Пользуясь шкалой градусов,  записать показания угла отклонения~$\varphi$  для жёлтой линии с точностью до минут. Провести измерения три раза. Усреднить полученные значения. Занести эти данные в таблицу~\ref{table1_05}.
\item Для призмы из стекла <<флинт>> провести измерения углов наименьшего отклонения для остальных линий ртути, каждый раз фокусируя окуляр на исследуемую спектральную линию (пункты~8-11).
\item Заменить призму из стекла <<флинт>> на призму из стекла <<крон>>.
\item Повторить измерения углов наименьшего отклонения для всех линий ртути и занести их в соответствующие столбцы таблицы~\ref{table1_05}.
\item Убрать призмы в коробки и выключить ртутную лампу.
\item Рассчитать углы наименьшего отклонения $\theta=\overline{\varphi} - \overline{\varphi}_0$. Здесь $\overline{\varphi}_0$~--- среднее значение угла нулевого отсчёта, $\overline{\varphi}$~--- среднее значение измеренного угла отклонения. Заполнить таблицу~\ref{table1_05}.
\item По формуле~(\ref{eq:nForPrizm_05}) рассчитать показатель преломления для каждой длины волны для обеих призм с точность до третьего знака после запятой. Преломляющий угол призмы $A$ равен $60^{\circ}$. Занести эти данные в таблицу~\ref{table2_05}.
\item Рассчитать значения $1/\lambda^2$. Длина волны спектральных линий ртути в таблице~\ref{table2_05} приведена в ангстремах (1 \AA{} = $10^{-8}$ см).
\item Построить графики зависимости $n$ от $\lambda$ для обеих призм. Сравнить полученные кривые с ожидаемым расчётом по формуле~(\ref{eq:formKoshi_05}), сделать вывод.
\item Построить графики зависимости $n$ от $\lambda$ для обеих призм.
\item Графическим или аналитическим способом найти дисперсию материала призм $\cfrac{dn}{d\lambda}$, внести эти данные в таблицу~\ref{table2_05}.
\item По формуле~(\ref{eq:uglovDisperFinal_05}) рассчитать угловую дисперсию обеих призм для всех длин волн. Заполнить таблицу~\ref{table2_05}.
\item Сделать вывод и оформить отчёт.
\end{enumerate}

\section{КОНТРОЛЬНЫЕ ВОПРОСЫ}
\begin{enumerate}
\item В чем заключается явление дисперсии света?
\item Чему равен показатель преломления вещества в теории Максвелла?
\item Какая   модель   строения   вещества   используется   в   электронной   теории дисперсии?
\item Напишите уравнение движения электрона в атоме под действием падающей световой  волны.  Какие  силы действуют на электрон?  Почему в уравнении отсутствует сила, действующая на электрон со стороны магнитного поля световой волны? Что находят из уравнения движения электрона в атоме?
\item Решите это уравнение и получите зависимость показателя преломления от частоты и длины волны падающего света. 
\item В каких случаях показатель преломления является комплексной величиной?
\item Как зависит показатель преломления прозрачных веществ от длины волны видимого света? Какому приближению это соответствует? Напишите формулу и нарисуйте график.
\item Для чего используются призменные спектральные приборы?
\item Что такое дисперсия вещества?
\item Дайте определение угловой дисперсии спектрального прибора. Чему равна угловая дисперсия призмы?
\item Как идут лучи в призме, если она установлена под углом наименьшего отклонения?
\end{enumerate}

\clearpage